package com.ecs.airdoot.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ecs.airdoot.R;

import org.json.JSONArray;
import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyTaskListRVAdapter extends RecyclerView.Adapter<MyTaskListRVAdapter.ViewHolder> {
    Context context;
    View itemView;
    JSONArray dataList;

    public MyTaskListRVAdapter(Context context, JSONArray dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_sr_no)
        TextView tvSrNo;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_address)
        TextView tvAddress;

        @BindView(R.id.tv_date_time)
        TextView tvDateTime;

        @BindView(R.id.tv_task_type)
        TextView tvTaskType;

        @BindView(R.id.spn_ac_type)
        Spinner SpnAcType;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_mytask_list, parent, false);

        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public Object getItem(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.tvSrNo.setText(String.valueOf(position + 1));
            holder.tvName.setText(dataList.getJSONObject(position).getString("name"));
            holder.tvAddress.setText(dataList.getJSONObject(position).getString("address"));
            holder.tvDateTime.setText(dataList.getJSONObject(position).getString("date_time"));
            holder.tvTaskType.setText(dataList.getJSONObject(position).getString("task_type"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return dataList.length();
    }
}
