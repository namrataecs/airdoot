package com.ecs.airdoot.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ecs.airdoot.R;

import org.json.JSONArray;
import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CommentsListRVAdapter extends RecyclerView.Adapter<CommentsListRVAdapter.ViewHolder> {
    Context context;
    View itemView;
    JSONArray dataList;

    public CommentsListRVAdapter(Context context, JSONArray dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_days_ago)
        TextView tvDaysAgo;

        @BindView(R.id.rb_rating)
        RatingBar rbRating;

        @BindView(R.id.tv_comments)
        TextView tvComments;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_comment_list, parent, false);

        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public Object getItem(int position) {
//        return dataList.get(position);
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.tvName.setText(dataList.getJSONObject(position).getString("person_name"));
            holder.tvDaysAgo.setText(dataList.getJSONObject(position).getString("day_ago"));
            holder.rbRating.setRating(Integer.parseInt(dataList.getJSONObject(position).getString("rating")));
            holder.tvComments.setText(dataList.getJSONObject(position).getString("comment"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return dataList.length();
    }
}
