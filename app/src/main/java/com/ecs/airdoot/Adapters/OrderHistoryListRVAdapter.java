package com.ecs.airdoot.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ecs.airdoot.Fragments.Technician.AddPhotosWorkOrderFragment;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.BasicUtils;

import org.json.JSONArray;
import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderHistoryListRVAdapter extends RecyclerView.Adapter<OrderHistoryListRVAdapter.ViewHolder> {
    Context context;
    View itemView;
    JSONArray dataList;

    public OrderHistoryListRVAdapter(Context context, JSONArray dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_sr_no)
        TextView tvSrNo;

        @BindView(R.id.tv_leakages)
        TextView tvLeakages;

        @BindView(R.id.tv_leakages_no)
        TextView tvLeakagesNo;

        @BindView(R.id.tv_resolved_issue)
        TextView tvResolvedIssue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_history_list, parent, false);

        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public Object getItem(int position) {
//        return dataList.get(position);
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.tvSrNo.setText(String.valueOf(position + 1));
            holder.tvLeakages.setText(dataList.getJSONObject(position).getString("leakages"));
            holder.tvLeakagesNo.setText(dataList.getJSONObject(position).getString("leakages_no"));
            holder.tvResolvedIssue.setText(dataList.getJSONObject(position).getString("resolved_issue"));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BasicUtils.replaceFragment(context, new AddPhotosWorkOrderFragment());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {

        return 2;
    }
}
