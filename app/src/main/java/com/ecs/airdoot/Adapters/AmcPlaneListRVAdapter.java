package com.ecs.airdoot.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ecs.airdoot.Fragments.AddToCartFragment;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.BasicUtils;

import org.json.JSONArray;
import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AmcPlaneListRVAdapter extends RecyclerView.Adapter<AmcPlaneListRVAdapter.ViewHolder> {
    Context context;
    View itemView;
    JSONArray dataList;

    public AmcPlaneListRVAdapter(Context context, JSONArray dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_items)
        TextView tvItems;

        @BindView(R.id.iv_comp)
        ImageView ivComp;

        @BindView(R.id.tv_comp)
        TextView tvComp;

        @BindView(R.id.iv_semi_comp)
        ImageView ivSemiComp;

        @BindView(R.id.tv_semi_comp)
        TextView tvSemiComp;

        @BindView(R.id.iv_non_comp)
        ImageView ivNonComp;

        @BindView(R.id.tv_non_comp)
        TextView tvNonComp;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_amc_plane_list, parent, false);

        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public Object getItem(int position) {
//        return dataList.get(position);
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            Log.e("array_data_items", ":" + dataList.getJSONObject(position).getString("item"));
            holder.tvItems.setText(dataList.getJSONObject(position).getString("item"));
            if (dataList.getJSONObject(position).getString("comp").equals("R")) {
                holder.ivComp.setBackgroundResource(R.drawable.right_mark);
            } else if (dataList.getJSONObject(position).getString("comp").equals("W")) {
                holder.ivComp.setBackgroundResource(R.drawable.wrong_mark);
            } else {
                holder.ivComp.setVisibility(View.GONE);
                holder.tvComp.setVisibility(View.VISIBLE);
                holder.tvComp.setText(dataList.getJSONObject(position).getString("comp"));

            }

            if (dataList.getJSONObject(position).getString("semi_comp").equals("R")) {
                holder.ivSemiComp.setBackgroundResource(R.drawable.right_mark);
            } else if (dataList.getJSONObject(position).getString("semi_comp").equals("W")) {
                holder.ivSemiComp.setBackgroundResource(R.drawable.wrong_mark);
            } else {
                holder.ivSemiComp.setVisibility(View.GONE);
                holder.tvSemiComp.setVisibility(View.VISIBLE);
                holder.tvSemiComp.setText(dataList.getJSONObject(position).getString("semi_comp"));

            }

            if (dataList.getJSONObject(position).getString("non_comp").equals("R")) {
                holder.ivNonComp.setBackgroundResource(R.drawable.right_mark);
            } else if (dataList.getJSONObject(position).getString("non_comp").equals("W")) {
                holder.ivNonComp.setBackgroundResource(R.drawable.wrong_mark);
            } else {
                holder.ivNonComp.setVisibility(View.GONE);
                holder.tvNonComp.setVisibility(View.VISIBLE);
                holder.tvNonComp.setText(dataList.getJSONObject(position).getString("non_comp"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return dataList.length();
    }
}
