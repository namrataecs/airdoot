package com.ecs.airdoot.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import com.ecs.airdoot.Adapters.AddPhotosOrderListRVAdapter;
import com.ecs.airdoot.Adapters.DashboardListRVAdapter;
import com.ecs.airdoot.Fragments.InspectionService.InspectionserviceFormFragment;
import com.ecs.airdoot.Fragments.Operational.OperationContractFormFragment;
import com.ecs.airdoot.Fragments.Retrofit.RetrofitFragment;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.BasicUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.ecs.airdoot.Activities.MainActivity.toolbar;


public class DashboardFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Context context;
    Unbinder unbinder;

    @BindView(R.id.lin_buy_amc)
    LinearLayoutCompat linBuyAms;

    @BindView(R.id.lin_retrofit)
    LinearLayoutCompat linRetrofit;

    @BindView(R.id.lin_operation_contract)
    LinearLayoutCompat linOperationContract;

    @BindView(R.id.lin_inspection_service)
    LinearLayoutCompat linInspectionService;

    @BindView(R.id.rv_dashboard)
    RecyclerView rv_dashboard;

    @BindView(R.id.sv_maincontent)
    ScrollView sv_maincontent;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DashboardFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.dashboard_layout, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        BasicUtils.setSidebar(getActivity(), toolbar);

        initializeData();
        setUpEvents();

        return rootView;
    }


    private void initializeData() {
        sv_maincontent.fullScroll(View.FOCUS_UP);
        sv_maincontent.pageScroll(View.FOCUS_UP);
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject;

            jsonObject = new JSONObject();
            jsonObject.put("leakages", "Water Leakages");
            jsonObject.put("leakages_no", "WO-N23454");
            jsonObject.put("resolved_issue", "2 Weeks Ago Resolved");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("leakages", "Not Cooling");
            jsonObject.put("leakages_no", "WO-N5643");
            jsonObject.put("Resolved_issue", "1 Month Ago Resolved");
            jsonArray.put(jsonObject);


            setItemListAdapter(jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private void setItemListAdapter(JSONArray HistoryArray) {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rv_dashboard.setLayoutManager(gridLayoutManager);
        DashboardListRVAdapter dashboardListRVAdapter = new DashboardListRVAdapter(context, HistoryArray);
        rv_dashboard.setAdapter(dashboardListRVAdapter);
    }

    private void setUpEvents() {
        linBuyAms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasicUtils.replaceFragment(context, new AmcFragment());
            }
        });

        linRetrofit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasicUtils.replaceFragment(context, new RetrofitFragment());
            }
        });

        linOperationContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasicUtils.replaceFragment(context, new OperationContractFormFragment());
            }
        });
        linInspectionService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasicUtils.replaceFragment(context, new InspectionserviceFormFragment());
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("onAttach_Log", "OnAttach");
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("onDetach_Log", "OnDetach");
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume_Log", "OnResume");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
