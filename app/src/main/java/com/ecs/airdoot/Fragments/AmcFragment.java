package com.ecs.airdoot.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ecs.airdoot.Adapters.AmcPlaneListRVAdapter;
import com.ecs.airdoot.Adapters.CommentsListRVAdapter;
import com.ecs.airdoot.Adapters.ServiceListRVAdapter;
import com.ecs.airdoot.Adapters.TonageListRVAdapter;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.BasicUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.ecs.airdoot.Activities.MainActivity.toolbar;


public class AmcFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Context context;
    Unbinder unbinder;

    @BindView(R.id.rv_tonage)
    RecyclerView rvTonage;

    @BindView(R.id.rv_service_per)
    RecyclerView rvServicePer;

    @BindView(R.id.rv_amc_plane)
    RecyclerView rvAmcPlane;

    @BindView(R.id.rv_comments)
    RecyclerView rvComments;

    @BindView(R.id.btn_add_to_cart)
    Button btnAddToCart;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AmcFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AmcFragment newInstance(String param1, String param2) {
        AmcFragment fragment = new AmcFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.amc_layout, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        BasicUtils.setSidebar(getActivity(), toolbar);

        initializeData();
        setUpEvents();

        return rootView;
    }


    private void initializeData() {
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject;

            jsonObject = new JSONObject();
            jsonObject.put("tonage1", "<1");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("tonage1", "1.2");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("tonage1", "1.5");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("tonage1", "1.8");
            jsonArray.put(jsonObject);
            jsonObject = new JSONObject();
            jsonObject.put("tonage1", "2");
            jsonArray.put(jsonObject);

            setItemListAdapter(jsonArray, "tonage");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject;

            jsonObject = new JSONObject();
            jsonObject.put("service_per_1", "4");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("service_per_1", "6");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("service_per_1", "12");
            jsonArray.put(jsonObject);

            setItemListAdapter(jsonArray, "service");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject;

            jsonObject = new JSONObject();
            jsonObject.put("item", "Compressor");
            jsonObject.put("comp", "R");
            jsonObject.put("semi_comp", "R");
            jsonObject.put("non_comp", "R");


            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("item", "Fan Motor");
            jsonObject.put("comp", "R");
            jsonObject.put("semi_comp", "R");
            jsonObject.put("non_comp", "R");

            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("item", "Refrigerant");
            jsonObject.put("comp", "R");
            jsonObject.put("semi_comp", "R");
            jsonObject.put("non_comp", "R");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("item", "Magnec");
            jsonObject.put("comp", "R");
            jsonObject.put("semi_comp", "R");
            jsonObject.put("non_comp", "W");
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("item", "Gap Top-up");
            jsonObject.put("comp", "R");
            jsonObject.put("semi_comp", "W");
            jsonObject.put("non_comp", "W");

            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("item", "Price");
            jsonObject.put("comp", "Rs 2000");
            jsonObject.put("semi_comp", "Rs 1800");
            jsonObject.put("non_comp", "Rs 1500");
            jsonArray.put(jsonObject);

            setItemListAdapter(jsonArray, "amc_plan");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject;

            jsonObject = new JSONObject();
            jsonObject.put("person_name", "Akash Pal");
            jsonObject.put("day_ago", "3 days ago");
            jsonObject.put("rating", "4");
            jsonObject.put("comment", getResources().getString(R.string.comments));
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("person_name", "Nish Kumar");
            jsonObject.put("day_ago", "10 days ago");
            jsonObject.put("rating", "4");
            jsonObject.put("comment", getResources().getString(R.string.comments));
            jsonArray.put(jsonObject);

            jsonObject = new JSONObject();
            jsonObject.put("person_name", "Rishi Gujar");
            jsonObject.put("day_ago", "1 month ago");
            jsonObject.put("rating", "5");
            jsonObject.put("comment",getResources().getString(R.string.comments));
            jsonArray.put(jsonObject);


            setItemListAdapter(jsonArray, "comments");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setUpEvents() {
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasicUtils.replaceFragment(context,new AddToCartFragment());
            }
        });
    }

    private void setItemListAdapter(JSONArray ServiceTonageAmcPlaneList, String type) {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false);
        if (type.equals("tonage")) {
            rvTonage.setLayoutManager(gridLayoutManager);
            TonageListRVAdapter tonageListRVAdapter = new TonageListRVAdapter(context, ServiceTonageAmcPlaneList);
            rvTonage.setAdapter(tonageListRVAdapter);

        } else if (type.equals("service")) {
            rvServicePer.setLayoutManager(gridLayoutManager);
            ServiceListRVAdapter serviceListRVAdapter = new ServiceListRVAdapter(context, ServiceTonageAmcPlaneList);
            rvServicePer.setAdapter(serviceListRVAdapter);

        } else if (type.equals("amc_plan")) {
            GridLayoutManager gridLayoutManager1 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
            rvAmcPlane.setLayoutManager(gridLayoutManager1);
            AmcPlaneListRVAdapter amcPlaneListRVAdapter = new AmcPlaneListRVAdapter(context, ServiceTonageAmcPlaneList);
            rvAmcPlane.setAdapter(amcPlaneListRVAdapter);

        } else {
            GridLayoutManager gridLayoutManager1 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
            rvComments.setLayoutManager(gridLayoutManager1);
            CommentsListRVAdapter commentsListRVAdapter = new CommentsListRVAdapter(context, ServiceTonageAmcPlaneList);
            rvComments.setAdapter(commentsListRVAdapter);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("onAttach_Log", "OnAttach");
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("onDetach_Log", "OnDetach");
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume_Log", "OnResume");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
