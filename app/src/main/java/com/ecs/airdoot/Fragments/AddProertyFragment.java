package com.ecs.airdoot.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ecs.airdoot.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddProertyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddProertyFragment extends Fragment {

    Context context;
    Unbinder unbinder;

    @BindView(R.id.spn_states)
    Spinner spn_states;

    @BindView(R.id.spn_countries)
    Spinner spn_countries;

    @BindView(R.id.spn_city)
    Spinner spn_city;

    @BindView(R.id.spn_area)
    Spinner spn_area;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddProertyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddProertyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddProertyFragment newInstance(String param1, String param2) {
        AddProertyFragment fragment = new AddProertyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_proerty, container, false);
        context=rootView.getContext();
        unbinder = ButterKnife.bind(this, rootView);

        initilizeData(context);
        return rootView;
    }

    private void initilizeData(Context context) {
        //spinner state adapter
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.states, R.layout.custom_spinner_layout);
        adapter.setDropDownViewResource(R.layout.custom_spinner_layout);
        spn_states.setAdapter(adapter);

        //spinner countries adapter
        ArrayAdapter<CharSequence> countriesAdapter = ArrayAdapter.createFromResource(context, R.array.countries, R.layout.custom_spinner_layout);
        countriesAdapter.setDropDownViewResource(R.layout.custom_spinner_layout);
        spn_countries.setAdapter(countriesAdapter);

        //spinner city adapter
        ArrayAdapter<CharSequence> cityAdapter = ArrayAdapter.createFromResource(context, R.array.city, R.layout.custom_spinner_layout);
        cityAdapter.setDropDownViewResource(R.layout.custom_spinner_layout);
        spn_city.setAdapter(cityAdapter);

        //spinner area adapter
        ArrayAdapter<CharSequence> areaAdapter = ArrayAdapter.createFromResource(context, R.array.area, R.layout.custom_spinner_layout);
        areaAdapter.setDropDownViewResource(R.layout.custom_spinner_layout);
        spn_area.setAdapter(areaAdapter);
    }
}