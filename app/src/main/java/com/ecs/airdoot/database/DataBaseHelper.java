package com.ecs.airdoot.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataBaseHelper {
    public static SQLiteDatabase sqLiteDatabase;
    private SQLiteHelper sqLiteHelper;
    public Context context;

    private JSONArray jArray;
    private JSONObject json_data;
    private boolean isExist = false;
    private String str_column_name;

    public DataBaseHelper(Context context) {
        this.context = context;
        sqLiteHelper = new SQLiteHelper(context);
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        sqLiteDatabase = sqLiteHelper.getReadableDatabase();
    }



    public Long saveToLocalTable(String table, ContentValues contentValues) {
        long count = 0;
        try {
            count = sqLiteDatabase.insert(table, null, contentValues);

            if (count != -1) {
                Log.v("DataHelp_Log", "Insert " + table + " Details Successfully");
            } else {
                Log.v("DataHelp_Log", "Insert " + table + " Details Fail");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }


    public JSONArray getUserDataByEmail(String email) {

        Cursor cursor = null;
        JSONArray jArray = new JSONArray();

        cursor = sqLiteDatabase.rawQuery("SELECT * from `" + DataBaseConstants.TableNames.TBL_USER_DETAILS + "` Where " + DataBaseConstants.Constants_TBL_USER_DETAILS.EMAIL_ADDRESS + " ='" + email + "'", null);
        Log.e("tbl_user_detailstable", ":" + cursor.getCount() + "  :   " + "SELECT * from `" + DataBaseConstants.TableNames.TBL_USER_DETAILS + "` Where " + DataBaseConstants.Constants_TBL_USER_DETAILS.EMAIL_ADDRESS + " ='" + email + "'");

        JSONObject json = null;

        if (cursor.getCount() != 0) {
            try {
                while (cursor.moveToNext()) {
                    json = new JSONObject();

                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        json.put(cursor.getColumnName(i), cursor.getString(cursor.getColumnIndex(cursor.getColumnName(i))));
                    }
                    jArray.put(json);
                }
                Log.e("get_user_array", ":" + jArray);
                return jArray;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (jArray.length() > 0)
            return jArray;

        return null;
    }

    public JSONArray getUserDataBynumber(String mobile_number,String password) {

        Cursor cursor = null;
        JSONArray jArray=null;
        jArray = new JSONArray();

        cursor = sqLiteDatabase.rawQuery("SELECT * from `" + DataBaseConstants.TableNames.TBL_USER_DETAILS + "` Where " + DataBaseConstants.Constants_TBL_USER_DETAILS.MOBILE_NUMBER + " ='" + mobile_number + "'" +" AND "+ DataBaseConstants.Constants_TBL_USER_DETAILS.PASSWORD + " ='" + password + "'", null);
        Log.e("tbl_user_detailstable", ":" + cursor.getCount() + "  :   " + "SELECT * from `" + DataBaseConstants.TableNames.TBL_USER_DETAILS + "` Where " + DataBaseConstants.Constants_TBL_USER_DETAILS.MOBILE_NUMBER + " ='" + mobile_number + "'" +" AND "+ DataBaseConstants.Constants_TBL_USER_DETAILS.PASSWORD + " ='" + password + "'");

        JSONObject json = null;

        if (cursor.getCount() != 0) {
            try {
                while (cursor.moveToNext()) {
                    json = new JSONObject();

                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        json.put(cursor.getColumnName(i), cursor.getString(cursor.getColumnIndex(cursor.getColumnName(i))));
                    }
                    jArray.put(json);
                }
                Log.e("get_user_array", ":" + jArray);
                return jArray;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (jArray.length() > 0)
            return jArray;

        return null;
    }


    public void deleteTable_data(String tbl_name, String key, String ids) {

        sqLiteDatabase.execSQL("delete from " + tbl_name + " WHERE  id =" + ids);

    }

    public void clearTable() {
        sqLiteDatabase.execSQL("delete from " + DataBaseConstants.TableNames.TBL_USER_DETAILS);
    }

}