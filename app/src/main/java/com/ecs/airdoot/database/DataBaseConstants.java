package com.ecs.airdoot.database;

/**
 * Class Name        :  <b>DataBaseConstants.java<b/>
 * Purpose           :  DataBaseConstants is class related of constants.
 * Developed By      :  <b>@Namrata Shukla</b>
 * Created Date      :  <b>9-11-2020</b>
 */


public class DataBaseConstants {

    public static final String DATABASE_NAME = "airdoot";
    public static final int DATABASE_VERSION = 1;

    public static class TableNames {
        public static final String TBL_USER_DETAILS = "tbl_user_details";
    }

    public static class Constants_TBL_USER_DETAILS {
        public static final String ID = "id";
        public static final String EMAIL_ADDRESS= "email_address";
        public static final String MOBILE_NUMBER= "mobile_number";
        public static final String PASSWORD= "password";
        public static final String OTP= "otp";
        public static final String CREATED_DATE_TIME = "created_date_time";
        public static final String UPDATED_BY = "updated_by";
        public static final String UPDATED_DATE_TIME = "updated_date_time";
        public static final String IS_DELETED = "is_deleted";
        public static final String CREATED_BY = "created_by";
        public static final String IS_UPLOADED = "is_uploaded";
        public static final String PHPID = "phpid";
    }

}