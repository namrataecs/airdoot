package com.ecs.airdoot.database;

public class SQLiteQueries {

    public static final String QUERY_TBL_PARAMETERS = "create table IF NOT EXISTS "
            + DataBaseConstants.TableNames.TBL_USER_DETAILS + "("
            + DataBaseConstants.Constants_TBL_USER_DETAILS.ID + " INTEGER primary key AUTOINCREMENT,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.EMAIL_ADDRESS + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.MOBILE_NUMBER + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.PASSWORD + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.OTP + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.CREATED_BY + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.CREATED_DATE_TIME + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.UPDATED_BY + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.UPDATED_DATE_TIME + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.IS_DELETED + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.IS_UPLOADED + " VARCHAR,"
            + DataBaseConstants.Constants_TBL_USER_DETAILS.PHPID + " VARCHAR" + ");";



}
