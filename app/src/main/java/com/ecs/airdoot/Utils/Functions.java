package com.ecs.airdoot.Utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.ColorRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Functions {
    DatePickerDialog datePickerDialog;

    public static void setDatePicker(Context context, final EditText et) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar newCalendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et.setText(year);
                //et.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();

    }
    public static void pbVisiblity(boolean status, ProgressBar pb_bar) {
        if (status) {
            pb_bar.setVisibility(View.GONE);
        } else {
            pb_bar.setVisibility(View.VISIBLE);
        }
    }

    public static void btnVisiblity(boolean status, ProgressBar pb_bar, Button btn) {
        if (status) {
            btn.setVisibility(View.VISIBLE);
            pb_bar.setVisibility(View.GONE);
        } else {
            btn.setVisibility(View.INVISIBLE);
            pb_bar.setVisibility(View.VISIBLE);
        }
    }


    public static void openWifiSettings(Context context) {

        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
        intent.setComponent(cn);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void setDatePickerAll(Context context, final EditText et) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar newCalendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                // et.setText(year);
                et.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();

    }

    public static boolean resultCheck(Context context, JSONObject response, View view) {
        try {
            String result = response.get("status").toString();
            if (TextUtils.equals(result, "1")) {
                return true;
            } else {
                resultFail(context, response, view);
                return false;
            }
        } catch (JSONException e) {
            jsonException(context, e, view);
            return false;
        }
    }

    public static void jsonException(Context context, JSONException e) {
        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public static void resultFail(Context context, JSONObject response, View view) {
        try {
            String msg = response.get("msg").toString();
            if (view instanceof TextView) {
                view.setVisibility(View.VISIBLE);
                ((TextView) view).setText(msg);
            } else {
                if (view == null) {
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
                }
            }

        } catch (JSONException e) {
            jsonException(context, e, view);
        }
    }

    public static void progressbar(boolean status, ProgressBar pb_bar) {
        if (status) {
            pb_bar.setVisibility(View.GONE);

        } else {
            pb_bar.setVisibility(View.VISIBLE);
        }
    }


    public static void jsonException(Context context, JSONException e, View view) {
        if (view instanceof TextView) {
            view.setVisibility(View.VISIBLE);
            ((TextView) view).setText(e.getMessage());
        } else {
            if (view == null) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Snackbar.make(view, e.getMessage(), Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    public static String twoDigitsInt(int i) {
        String s_i = "";
        if (i < 10) {
            s_i = "0" + i;
        } else {
            s_i = "" + i;
        }
        return s_i;
    }

    private static void setTextColorForMenuItem(MenuItem menuItem, @ColorRes int color, Context context) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), 0, spanString.length(), 0);
        menuItem.setTitle(spanString);
    }

    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy, hh:mm a");
        Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
        String s_time = dateFormat.format(currentCalender.getTime());
        return s_time;
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void setTimePicker(Context context, final EditText et) {
        final TimePickerDialog timePickerDialog;
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = mcurrentTime.get(Calendar.MINUTE);
        timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int mHour = selectedHour;
                int mMin = selectedMinute;

                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";

                } else {
                    AM_PM = "PM";
                    mHour = mHour - 12;
                }
                et.setText(mHour + ":" + mMin + " " + AM_PM);

            }
        }, hour, minute, false);

        timePickerDialog.show();
    }

    public static void setAdapterwithposition(Context context, String[] list, Spinner spinner, String value) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, list);
        spinner.setAdapter(adapter);
        for (int i = 0; i < list.length; i++) {
            String valuez = adapter.getItem(i).toString();
            if (TextUtils.equals(value, valuez)) {
                int spinnerPosition = adapter.getPosition(valuez);
                spinner.setSelection(spinnerPosition, false);
            }

        }


    }

    public static void setAdapterwithpositionedittext(Context context, String[] list, Spinner spinner, String value, EditText editText, LinearLayout linearLayout) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, list);
        spinner.setAdapter(adapter);
        for (int i = 0; i < list.length; i++) {
            String valuez = adapter.getItem(i).toString();
            if (TextUtils.equals(value, valuez)) {
                int spinnerPosition = adapter.getPosition(valuez);
                spinner.setSelection(spinnerPosition, false);
            } else {
                spinner.setSelection(7, false);
                editText.setText(value);
                linearLayout.setVisibility(View.VISIBLE);
            }

        }


    }


    public static void detailsDialog(final Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "  CLOSE  ",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        alertDialog.show();
    }

    public static void startNewActivity(Context context, Class<Activity> activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
    }


   /* public static void exitApp(final Context context, String str_msg, String language, SharedPreferencesDatabase sharedPreferencesDatabase) {
        sharedPreferencesDatabase = new SharedPreferencesDatabase(context);
        sharedPreferencesDatabase.createDatabase();
        if (language.equals(Config.LANGUAGE_HINDI)) {


            final SharedPreferencesDatabase finalSharedPreferencesDatabase = sharedPreferencesDatabase;
            new AlertDialog.Builder(context)
                    .setMessage(str_msg)
                    .setCancelable(false)
                    .setPositiveButton("हाँ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            ((Activity) context).finish();

                            finalSharedPreferencesDatabase.removeDataByKey(Config.KEY_LOGIN_STATUS);

                        }
                    })
                    .setNegativeButton("नहीं", null)
                    .show();

        } else if (language.equals(Config.LANGUAGE_ENGLISH)) {

            final SharedPreferencesDatabase finalSharedPreferencesDatabase = sharedPreferencesDatabase;
            new AlertDialog.Builder(context)
                    .setMessage(str_msg)
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(context, SelectLoginRegisterActivity.class);
                            context.startActivity(intent);
                            ((Activity) context).finish();

                            finalSharedPreferencesDatabase.removeDataByKey(Config.KEY_LOGIN_STATUS);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }

    }*/

    public void setStatusBarColor(Context context, View statusBar, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = ((AppCompatActivity) context).getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            statusBar.setBackgroundColor(color);
        }
    }

    public static boolean Validatestring(String str) {
        boolean val = true;
        if (str != null && !str.isEmpty() && !str.equals("null")) {
            val = false;
        } else {
            val = true;
        }
        return val;
    }

    public static void setDatatoRecyclerViewa(RecyclerView recyclerView, RecyclerView.Adapter adapter, Context context, boolean value) {
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        if (value) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(layoutManager);
        } else {
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
            recyclerView.setLayoutManager(layoutManager);
        }

        recyclerView.setAdapter(adapter);

    }

    public void CompareDates(Context context) throws Exception {
        String dateString = "10-11-2000";
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 21);
        Toast.makeText(context, "User is not complete 21 years", Toast.LENGTH_SHORT).show();
        //Toast.makeText(, dateString + "" + calendar.getTime().after(date), Toast.LENGTH_LONG).show();
        System.out.printf("Date %s is older than 18? %s", dateString, calendar.getTime().after(date));
    }

    public static void setDatatoRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter, Context context) {
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    public static void message(Context context, String msg, View view) {
        if (view instanceof TextView) {
            view.setVisibility(View.VISIBLE);
            ((TextView) view).setText(msg);
        } else {
            if (view == null) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            } else {
                Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    public static String nullConvert(String s) {
        if (TextUtils.equals(s, "null")) {
            return "";
        } else {
            return s;
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showkeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void setyear(final Context context, final EditText yearMonth) {
        int mYear, mMonth, mDay;
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        view.getYear();

                        yearMonth.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    public static void createView(Context context, int count) {
        if (count > 0) {

            //mParentLayout.addView(view);
        }
    }


}


   /* public static void order_successfull_dialog(final Context context) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.dialog_order_placed, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        //alertDialogBuilder.setTitle("Update Cart");
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        Button btn_ok = promptsView.findViewById(R.id.btnOrderSuccessfully);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DrawerActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });
        // set dialog message
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        // show it
        alertDialog.show();


    }*/

