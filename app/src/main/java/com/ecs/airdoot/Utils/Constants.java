package com.ecs.airdoot.Utils;

import android.Manifest;


public class Constants {


    public static String BASE_URL = "http://airdoot.aaartise.com/Webservices/Android_service/";
    public static final String SERER_CHECK_URL = "";
    public static final String SERER_CHECK_URL_GHATGROUP = "";
    public static String IMAGE_BASE_URL = "";
    public static String LOGIN_CHECK = BASE_URL + "login_check";
    public static String CHECK_AVAILABILITY_EMAIL = BASE_URL + "check_availability_email";
    public static String CHECK_AVAILABILITY_CONTACT_NO = BASE_URL + "check_availability_contact_no";
    public static final String TBL_LOGIN = BASE_URL + "UploadData/tbl_login";
    public static String CHECK_EXISTING_USER= "";
    public static String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION};

    public static String SUCCESS_STATUS_CODE = "200";
    public static String ERROR_STATUS_CODE = "515";
    public static String ERROR_EXCEPTION_CODE = "100";
    public static String SUCCESS = "success";
    public static String NO_DATA_FOUND = "Records not available, Please try after some time";
    public static String SERVER_ERROR = "Server error, Please try after some time...";
    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;


}
