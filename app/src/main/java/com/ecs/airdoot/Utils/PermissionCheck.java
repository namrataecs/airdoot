package com.ecs.airdoot.Utils;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static com.ecs.airdoot.Utils.Constants.PERMISSIONS_REQUEST_READ_CONTACTS;


public class PermissionCheck {

    public static boolean checkLocationPermission(final Context context) {

        if (ContextCompat.checkSelfPermission(context
                ,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(((AppCompatActivity) context),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(context
                )
                        .setTitle("Location Permission")
                        .setMessage("Requesting Location Permission to get your current location to show nearby businesses")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((AppCompatActivity) context
                                        ,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        1);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(((AppCompatActivity) context)
                        ,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            }
            return false;
        } else {
            checkCallPermission(context);
            return true;
        }
    }

    public static boolean checkContactPermission(final Context context) {

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(((AppCompatActivity) context)
                    ,
                    Manifest.permission.READ_CONTACTS)) {
                new AlertDialog.Builder(context
                )
                        .setTitle("Contact Permission")
                        .setCancelable(false)
                        .setMessage("Requesting Contact Permission to get your Contact List")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((AppCompatActivity) context
                                        ,
                                        new String[]{Manifest.permission.READ_CONTACTS},
                                        PERMISSIONS_REQUEST_READ_CONTACTS);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(((AppCompatActivity) context)
                        ,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        1);
            }
            return false;
        } else {
//            checkCallPermission(context);
            return true;
        }
    }


    public static boolean checkCallPermission(final Context context) {
        if (ContextCompat.checkSelfPermission(context
                ,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(((AppCompatActivity) context)
                    ,
                    Manifest.permission.CALL_PHONE)) {
                new AlertDialog.Builder(context
                )
                        .setTitle("Call Permission")
                        .setMessage("Requesting Call Permission to allow you to directly call to the business")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((AppCompatActivity) context
                                        ,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        3);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions((AppCompatActivity) context
                        ,
                        new String[]{Manifest.permission.CALL_PHONE},
                        3);
            }
            return false;
        } else {
            checkStorageReadPermission(context);
            return true;
        }
    }

    public static boolean checkStorageReadPermission(final Context context) {
        if (ContextCompat.checkSelfPermission(context
                ,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((AppCompatActivity) context
                    ,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(context
                )
                        .setTitle("Storage Read Permission")
                        .setMessage("Requesting Storage Read Permission to upload photos on website")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((AppCompatActivity) context
                                        ,
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                        4);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions((AppCompatActivity) context
                        ,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        4);
            }
            return false;
        } else {
            checkStorageWritePermission(context);
            return true;
        }
    }

    public static boolean checkStorageWritePermission(final Context context) {
        if (ContextCompat.checkSelfPermission(context
                ,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((AppCompatActivity) context
                    ,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(context
                )
                        .setTitle("Storage Write Permission")
                        .setMessage("Requesting Storage Write Permission to save photo to the phone")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(((AppCompatActivity) context)
                                        ,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        5);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions((AppCompatActivity) context
                        ,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        5);
            }
            return false;
        } else {
            return true;
        }
    }

}
