package com.ecs.airdoot.Utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateTime {

    public static final int FLAG_ONLY_NEW = 1;
    public static final int FLAG_OLD_AND_NEW = 2;
    public static final int FLAG_OLD_ONLY = 3;

    public static String getFromMillies(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date(time));
    }

    public static String getFromMillies(long time, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(time));
    }

    public static Date getStringToDate(String date) {

        Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date1;
    }

    public static String getFromMilliesInUTC(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(new Date(time));
    }

    public static String getFromMilliesInUTC(long time, String formate) {
        SimpleDateFormat format = new SimpleDateFormat(formate);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(new Date(time));
    }

    public static String getDateFromUTC(String datetime, String formatFrom, String formatTo) throws ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat(formatFrom);
        SimpleDateFormat sdf2 = new SimpleDateFormat(formatTo);
//        sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
        sdf1.setTimeZone(TimeZone.getTimeZone(getCurrentTimeZone()));
        return sdf2.format(sdf1.parse(datetime));
    }


    public static long getInMilliesFromUTC(String time) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.parse(time).getTime();
    }

    public static long getInMillies(String time) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm a");
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.parse(time).getTime();
    }

    public static long getInMillies(String time, String formate)
            throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(formate);
        return format.parse(time).getTime();
    }

    public static String getNowInUTC() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(new Date(System.currentTimeMillis()));
    }

    public static String getNow() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date(System.currentTimeMillis()));
    }

    public static String getCurrentDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(new Date(System.currentTimeMillis()));
    }

    public static String getCurrentTimeZone() {
        TimeZone timeZone = TimeZone.getDefault();
        return timeZone.getID();
    }

    public static String getDate(String datetime) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        return format2.format(format1.parse(datetime));
    }

    public static String getDate(Date datetime) throws ParseException {
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        return format2.format(datetime);
    }

    public static String getTime(String datetime) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm:ss");
        return format2.format(format1.parse(datetime));
    }

    public static String getDateTime(String datetime, String formatFrom,
                                     String formatTo) throws ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat(formatFrom);
        SimpleDateFormat sdf2 = new SimpleDateFormat(formatTo);
        return sdf2.format(sdf1.parse(datetime));
    }

    public static String convertDateTimeBwTimeZone(String datetime,
                                                   String originalTimezone, String finalTimeZone)
            throws ParseException {
        String patternString = "yyyy-MM-dd HH:mm:ss";
        return convertDateTimeBwTimeZone(datetime, patternString,
                originalTimezone, finalTimeZone);
    }

    public static int getDaysInTwoDate(String date) {
        Date userDob = null;
        try {
            userDob = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date today = new Date();
        Log.e("today : selected ", today.getTime() + "  :  " + userDob.getTime());
        long diff = today.getTime() - userDob.getTime();
        int numOfYear = (int) ((diff / (1000 * 60 * 60 * 24)) / 365);
        int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
        int hours = (int) (diff / (1000 * 60 * 60));
        int minutes = (int) (diff / (1000 * 60));
        int seconds = (int) (diff / (1000));

        //Log.e("hours", ""+hours);
        return numOfDays;
    }

   public static int getDifferenceInTwoTime(String fromDate, String toDate) {
        Date frmDate = null;
        Date today = null;

        try {
            frmDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fromDate);
            today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(toDate);

            Log.e("FromDateDateTime",":"+fromDate);
            Log.e("toDateDateTime",":"+toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        Date today = new Date();
        Log.e("today : selected ", today.getTime() + "  :  " + frmDate.getTime());

        long diff = today.getTime() - frmDate.getTime();
        int numOfYear = (int) ((diff / (1000 * 60 * 60 * 24)) / 365);
        int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
        int hours = (int) (diff / (1000 * 60 * 60));
        int minutes = (int) (diff / (1000 * 60));
        int seconds = (int) (diff / (1000));

        Log.e("hoursDiff", ""+hours);
        Log.e("minutesDiff", ""+minutes);
        return minutes;
    }

    public static String getWeekDay(String date) {
        Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Then get the day of week from the Date based on specific locale.
        String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date1);

        return dayOfWeek;
    }

    public static String formatDate(String date1) {
        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy");
        Date d = null;
        try {
            d = df2.parse(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String date = df3.format(d);


        Log.e("formatDate", date1 + " : " + date);

        return date;
    }

    public static String getMonth(int month) {

        SimpleDateFormat newformat = new SimpleDateFormat("MMM");
        SimpleDateFormat oldformat = new SimpleDateFormat("MM");

        String monthName = null;
        Date myDate;
        try {
            myDate = oldformat.parse(String.valueOf(month));
            monthName = newformat.format(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthName;
    }

    public static String convertTime(String time) throws ParseException {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = displayFormat.parse(time);
        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
        return parseFormat.format(date);
    }

    public static String convertTo24hrsTime(String time) throws ParseException {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = parseFormat.parse(time);
        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
        return displayFormat.format(date);
    }

    public static String convertDateTimeBwTimeZone(String datetime,
                                                   String patternString, String originalTimezone, String finalTimeZone)
            throws ParseException {
        try {
            DateFormat originalFormate = new SimpleDateFormat(patternString);
            originalFormate.setTimeZone(TimeZone.getTimeZone(originalTimezone));

            DateFormat finalFormat = new SimpleDateFormat(patternString);
            finalFormat.setTimeZone(TimeZone.getTimeZone(finalTimeZone));

            Date timestamp = originalFormate.parse(datetime);
            String output = finalFormat.format(timestamp);
            return output;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datetime;
    }

    public static String getToday() {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(calendar.getTime());
    }

    public static Calendar getToday1() {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return calendar;
    }

    public static String getThisMonthFirstDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(calendar.getTime());
    }

    public static String getThisMonthLastDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(calendar.getTime());
    }

    public static int getDayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return day;
    }

    /**
     * ISO 8601 strings
     * (in the following format: "2008-03-01T13:00:00+01:00"). It supports
     * parsing the "Z" timezone, but many other less-used features are
     * missing.
     */
    /**
     * Transform Calendar to ISO 8601 string.
     */
    private static String fromCalendar(final Calendar calendar) {
        Date date = calendar.getTime();
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .format(date);
        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }

    /**
     * Get current date and time formatted as ISO 8601 string.
     */
    public static String getNowInISO() {
        return fromCalendar(Calendar.getInstance());
    }

    /**
     * Get current date and time formatted as ISO 8601 string.
     */
    public static String getInISO(Calendar calendar) {
        return fromCalendar(calendar);
    }

    /**
     * Transform ISO 8601 string to Calendar.
     */
    public static Calendar toCalendarFromISO(final String iso8601string)
            throws ParseException {
        Calendar calendar = GregorianCalendar.getInstance();
        String s = iso8601string.replace("Z", "+00:00");
        try {
            s = s.substring(0, 22) + s.substring(23);  // to get rid of the ":"
        } catch (IndexOutOfBoundsException e) {
            throw new ParseException("Invalid length", 0);
        }
        Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s);
        calendar.setTime(date);
        return calendar;
    }

    public static String getDayMonth1(String date1) throws ParseException {
        SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = displayFormat.parse(date1);

        SimpleDateFormat df3 = new SimpleDateFormat("MMM, yy");
        return df3.format(date);
    }


    public static Date getStringToDateTime(String date) {

        Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1;
    }


    public static Date getDateNow() {
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return new Date(System.currentTimeMillis());
    }


    public static String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
    }


}
