package com.ecs.airdoot.Utils;

import android.content.Context;
import android.widget.Toast;


public class MU {

    public static final String NO_INTERNET_CONNECTION_TRY_AGAIN = "No internet conncetion. Please Try again";

    public static String ENTER_USER_NAME = "Please enter user name";
    public static String ENTER_NEW_PASS = "Please enter password";
    public static String ENTER_FIRST_NAME = "Please enter first name";
    public static String ENTER_LAST_NAME = "Please enter last name";
    public static String ENTER_SELECT_DATE = "Please select birth date";
    public static String ENTER_EMAIL_ADDRESS = "Please enter email address";
    public static String ENTER_EMAIL_DESCRIPTION = "Please enter description";
    public static String ENTER_VALID_EMAIL_ADDRESS = "Please enter valid email address";
    public static String ENTER_PASSWORD= "Please enter password";
    public static String ENTER_CONFIRM_PASSWORD= "Please enter confirm password";
    public static String PLEASE_SELECT_PROFILE_PICTURE= "Please select profile picture.";
    public static String CONFIRM_PASSWORD_MISMATCH= "Confirm password dose not matched with entered password";
    public static String ENTER_REGISTERED_EMAIL_ADDRESS = "Please enter registered email address";
    public static String ENTER_VERIFICATION_NO = "Please enter verification number received on email.";
    public static String ENTER_NEW_PASSWORD = "Please enter new password";
    public static String ENTER_OLD_PASSWORD = "Please enter old password";
    public static String CONFIRM_PASSWORD_MISMATCH_WITH_NEW_PASSWORD= "Confirm password dose not matched new with entered password";


    public static void ShowToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
