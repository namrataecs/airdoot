package com.ecs.airdoot.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FU {
    // TODO Flag of the date piker and time piker
    public static final int INSTER = 1;
    public static final int SELECT = 2;
    public static final int UPDATE = 3;
    public static final int DELETE = 4;

    public static final int SELECT_WHERE_ALL = 1;
    public static final int SELECT_WHERE_ID = 2;
    public static final int SELECT_WHERE_DATE_NAME = 3;
    public static final int SELECT_WHERE_NAME = 4;
    public static final int SELECT_WHERE_DATE = 5;

    public static final int FLAG_ONLY_NEW = 1;
    public static final int FLAG_OLD_AND_NEW = 2;
    public static final int FLAG_OLD_ONLY = 3;

    public static final int IN = 1;
    public static final int OUT = 2;
    public static final int ALL = 3;


    //TODO Added for Groupool
    public static final int FROM = 0;
    public static final int TO = 1;
    public static final String CAR_OTHER = "OT";
    public static final String CAR_SEDEN = "SE";
    public static final String CAR_HACHBACK = "HB";
    public static final String CAR_SUV = "SU";
    // TODO: 23/06/15 added
    //RE, JG(joined gorup), JO (joined) OF, SY
    public static final String REQUEST = "RE";
    public static final String JOIN_GROUP = "JG";
    public static final String JOIN_RIDE = "JO";
    public static final String OFFER_RIDE = "OF";
    public static final String SYSTEM = "SY";

    public static class AllActivity {
        public static final int TO = 1;
        public static final String BACK = "BACK";
        public static final String DOWNLOAD_ALL = "DOWNLOAD_ALL";
        public static final String FB_DATA = "fb_data";
        public static final String OTP_REQUEST = "request";
        public static final String OTP_REQUEST_CALL_BY_FB = "request_call_by_fb";
        public static final String OTP_REQUEST_ACTIVITY = "activity";
        public static final String MainActivity = "DashBoardActivity";
        public static final String LoginActivity = "SelectLoginRegisterActivity";
        public static final String MainLoginActivity = "MainLoginActivity";

        public static final String Name = "Name";
        public static final String ID = "Id";
        public static final String PROFILE_PICTURE_URI = "ProfilePictureUri";
    }

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    public static String ConverstionTime(String appTIME) {
        String convertedTime = "";
        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            Date date = null;
            date = parseFormat.parse(appTIME);
            Log.e("appo_time", ":)" + String.valueOf(displayFormat.format(date)));
            System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
            convertedTime = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime;
    }

}
