package com.ecs.airdoot.Utils;

import android.util.Log;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtils {

    public static String getDate() {

        long time = System.currentTimeMillis();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(time);
        return formatter.format(date);
    }

    public static String getTime() {

        long time = System.currentTimeMillis();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(time);
        return formatter.format(date);
    }

    public static String getTimeFromEpoch(long time) {
        time = time * 1000;
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        Date date = new Date(time);
        return formatter.format(date);
    }

    public static String getDateFromEpoch(long time) {
        time = time * 1000;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(time);
        return formatter.format(date);
    }

    public static long getEpochFromTime(String time) {

        long epoch = 0;
        String[] timesplit = time.split(":");

        long hour = Long.parseLong(timesplit[0]) * 3600;
        long min = Long.parseLong(timesplit[1]) * 60;

        epoch = hour + min;
        return epoch;
    }

    public static int convertTimeToMinutes(String time) {
        int minutes = 0;
        String[] timeSplit = time.split(":");
        int hr = Integer.parseInt(timeSplit[0]);
        int min = Integer.parseInt(timeSplit[1]);
        minutes = (hr * 60) + min;
        return minutes;
    }

    public static String convertMinutesToTime(int minutes) {
        boolean neg = false;
        if (minutes < 0) {
            minutes = minutes * (-1);
            neg = true;
        }
        int hour = minutes / 60;
        int min = minutes % 60;
        DecimalFormat formatter = new DecimalFormat("00");
        String time = formatter.format(hour) + ":" + formatter.format(min);
        if (neg) {
            time = "-" + time;
        }
        return time;
    }

    public static String calculateTimeDifference(String startTime, String endTime) {
        String time = "";

        Log.e("startTime_TimeUtils", ":" + startTime + "   :   " + endTime);

        String[] startTimeSplit = startTime.split(":");
        String[] endTimeSplit =endTime.split(":");

        int minutes = Integer.parseInt(endTimeSplit[1]) - Integer.parseInt(startTimeSplit[1]);
        int hour = Integer.parseInt(endTimeSplit[0]) - Integer.parseInt(startTimeSplit[0]);

        if (minutes < 0) {
            minutes = 60 + minutes;
            hour = hour - 1;
        }
        DecimalFormat formatter = new DecimalFormat("00");
        time = formatter.format(hour) + ":" + formatter.format(minutes);
        return time;
    }

    public static String addTime(String time, String time2) {

        String[] timeSplit = time.split(":");
        String[] timeSplit2 = time2.split(":");

        int minutes = Integer.parseInt(timeSplit[1]) + Integer.parseInt(timeSplit2[1]);
        int hour = Integer.parseInt(timeSplit[0]) + Integer.parseInt(timeSplit2[0]);

        if (minutes > 59) {
            minutes = minutes - 60;
            hour = hour + 1;
        }
        DecimalFormat formatter = new DecimalFormat("00");
        return formatter.format(hour) + ":" + formatter.format(minutes);
    }

    public static String convertSecondsToGMTTime(long seconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String gmtStrDate = sdf.format(seconds);
        return gmtStrDate;
    }

    public static String convertSecondsToTime(long seconds) {
        long min = seconds / 60;
        long sec = seconds % 60;
        long hr = min / 60;
        min = min % 60;

        DecimalFormat formatter = new DecimalFormat("00");
        return formatter.format(hr) + ":" + formatter.format(min);
    }

    public static int compareTime(String time, String time2) {
        int hr = Integer.parseInt(time.split(":")[0]);
        int min = Integer.parseInt(time.split(":")[1]);
        int hr2 = Integer.parseInt(time2.split(":")[0]);
        int min2 = Integer.parseInt(time2.split(":")[1]);

        int hrDiff = hr - hr2;
        int minDiff = min - min2;

        return hrDiff * 60 + minDiff;

    }

    public static String getTodayStartEpoch() {
        long epoch = 1553558400;
        long diff = System.currentTimeMillis() / 1000 - epoch;
        long times = diff / 86400;
        long newEpoch = 86400 * times + epoch;
        return String.valueOf(newEpoch);
    }
}
