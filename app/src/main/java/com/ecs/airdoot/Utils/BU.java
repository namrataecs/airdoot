package com.ecs.airdoot.Utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.viewpager.widget.ViewPager;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Scanner;

public class BU {
    private static final String TAG = "BU";
    public static int POSITION = 0;
    public static int HEALTH_RECORD_POSITION = 0;
    public static ViewPager viewPager = null;
    public static final String GCM_PROJECT_ID = "653329561144"; //TODO : ECS Dev ID
    public static View rootView;

    public static boolean isConnectingToInternet(Context appContext) {
        // Method to check internet connection
        ConnectivityManager connectivity = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
//        LTU.TIS(appContext, TAG, ACU.MSG.NO_INTERNET);
        //LTU.TIS(appContext, "ECS", "No Internet access.");


        return false;
    }

    public static String getIMEI(Context context) {
        String identifier = null;
        TelephonyManager tm;
        try {
            tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null) {
                identifier = tm.getDeviceId();
            }
            if (identifier == null || identifier.length() == 0) {
                identifier = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            }
        } catch (Exception e) {
            return "Not Available";
        }
        return identifier;

    }

    public static String getLocalIpAddress() {
        // AndroidManifest.xml permissions
        // <uses-permission android:name="android.permission.INTERNET" />
        // <uses-permission
        // android:name="android.permission.ACCESS_NETWORK_STATE" />
        // TODO get IP Address
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress
                                .hashCode());
                        // Log.i(TAG, "***** IP="+ ip);
                       // LTU.LI("BU getLocalIpAddress", "IP Address :" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
           // LTU.LE("BU getLocalIpAddress", ex.toString());
        }
        return "NA";
    }

//    public static String getIPAddress(boolean useIPv4) {
//
//        try {
//            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
//            for (NetworkInterface intf : interfaces) {
//                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
//                for (InetAddress addr : addrs) {
//                    if (!addr.isLoopbackAddress()) {
//                        String sAddr = addr.getHostAddress().toUpperCase();
//                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
//                        if (useIPv4) {
//                            if (isIPv4)
//                                return sAddr;
//                        } else {
//                            if (!isIPv4) {
//                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
//                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception ex) {
//        } // for now eat exceptions
//        return "";
//    }

    public void get(Context context) {
//        TelephonyManager tm = (TelephonyManager)getSystemService(context.TELEPHONY_SERVICE);
//        String countryCode = tm.getNetworkCountryIso();

    }

    public static void hideKeyboard(Activity v) {
        // Check if no view has focus:
        View view = v.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) v
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        //original distance
        //dist = dist * 1.609344;

        //duplicate distance
        dist = dist * 2.30;
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    private static double calaculateBMI(){
        final double KILOGRAMS_PER_POUND = 0.453;
        final double METERS_PER_INCH = 0.026;

        Scanner input = new Scanner(System.in);

        System.out.print("Enter weight in pounds: ");
        double weight = input.nextDouble();

        System.out.print("Enter height in inches: ");
        double height = input.nextDouble();

        double weightInKilogram = weight * KILOGRAMS_PER_POUND;
        double heightInMeters = height * METERS_PER_INCH;
        double bmi = weightInKilogram /
                (heightInMeters * heightInMeters);

        return bmi;
    }

    public static double getHeightFeetToMeter(double height) {
        double meters = height/3.2808;
        return meters;
    }

    public static double getHeightCMToMeter(double height) {
        double meters = (double) height / 100;
        return meters;
    }

    public static double getWeightPoundsToKg(double weight) {
        //double kilograms = weight * 0.454;
        double kilograms = Math.round((weight * .454) * 10) /10.0;
        return kilograms;
    }


    public static String getHeightInCm(String feet_inch) {

        double dCentimeter = 0d;

        if(!TextUtils.isEmpty(feet_inch)){
            if(feet_inch.contains("'")){
                String[] result1 = feet_inch.split("'");

                if(result1.length>0) {
                    String tempfeet = result1[0];

                    if (!TextUtils.isEmpty(tempfeet)) {
                        dCentimeter += ((Double.valueOf(tempfeet)) * 30.48);
                    }
                }
                if(result1.length>1) {
                    String tempinch = result1[1];
                    if (!TextUtils.isEmpty(tempinch)) {
                        dCentimeter += ((Double.valueOf(tempinch)) * 2.54);
                    }
                }
            }
        }
        return String.valueOf(dCentimeter);
    }

    public static String getHeightInFeet(String centemeter) {
        int feetPart = 0;
        int inchesPart = 0;
        if(!TextUtils.isEmpty(centemeter)) {
            double dCentimeter = Double.valueOf(centemeter);
            feetPart = (int) Math.floor((dCentimeter / 2.54) / 12);
            System.out.println((dCentimeter / 2.54) - (feetPart * 12));
            inchesPart = (int) Math.ceil((dCentimeter / 2.54) - (feetPart * 12));
        }
        return String.format("%d %d", feetPart, inchesPart);
    }

    public static double getWeightKgToPounds(double weight) {
        double pounds = weight * 2.2;
        return pounds;
    }

    public static double getHeightMeterToinches(double height) {
        double height_in_inch = height / 0.0254;
        return height_in_inch;
    }

}

