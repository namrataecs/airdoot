package com.ecs.airdoot.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    public static final String PREF_NAME = "call_a_doc";
    public static final String IS_LOGGED_IN = "logged_in";

    public static final String STATUS = "status";
    public static final String ID = "id";
    public static final String USER_TYPE = "user_type";
    public static final String APPLICATION_ID = "application_id";
    public static final String ORG_NAME = "org_name";
    public static final String EMAIL_ID = "email_id";
    public static final String PASSWORD = "password";
    public static final String CONTACT_NO = "contact_no";
    public static final String USER_SOCIAL_ACCOUNT_TYPE = "user_social_account_type";

    private static SharedPreferences preference;
    private static PreferenceManager preferenceManager = null;
    public Context context;

    public PreferenceManager(Context context) {
        this.context = context;
        preference = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static PreferenceManager getInstance(Context context) {
        if (preferenceManager == null) {
            preferenceManager = new PreferenceManager(context);
        }
        return preferenceManager;
    }

    public void clear() {
        preference.edit().clear().apply();
    }

    public boolean getIsLoggedIn() {
        return preference.getBoolean(IS_LOGGED_IN, false);
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        preference.edit().putBoolean(IS_LOGGED_IN, isLoggedIn).apply();
    }


    public void setId(String id) {
        preference.edit().putString(ID, id).apply();
    }

    public String getId() {
        return preference.getString(ID, "");
    }

    public void setStatus(String status) {
        preference.edit().putString(STATUS, status).apply();
    }

    public String getStatus() {
        return preference.getString(STATUS, "");
    }

    public void setUserType(String userType) {
        preference.edit().putString(USER_TYPE, userType).apply();
    }

    public String getUserType() {
        return preference.getString(USER_TYPE, "");
    }

    public void setApplicationId(String applicationId) {
        preference.edit().putString(APPLICATION_ID, applicationId).apply();
    }

    public String getApplicationId() {
        return preference.getString(APPLICATION_ID, "");
    }

    public void setOrgName(String org_name) {
        preference.edit().putString(ORG_NAME, org_name).apply();
    }

    public String getOrgName() {
        return preference.getString(ORG_NAME, "");
    }


    public void setContactNo(String contact_no) {
        preference.edit().putString(CONTACT_NO, contact_no).apply();
    }

    public String getContactNo() {
        return preference.getString(CONTACT_NO, "");
    }

    public void setPassword(String password) {
        preference.edit().putString(PASSWORD, password).apply();
    }

    public String getPassword() {
        return preference.getString(PASSWORD, "");
    }

    public void setEmailId(String email_id) {
        preference.edit().putString(EMAIL_ID, email_id).apply();
    }

    public String getEmailId() {
        return preference.getString(EMAIL_ID, "");
    }

    public void setUserSocialAccountType(String user_social_account_type) {
        preference.edit().putString(USER_SOCIAL_ACCOUNT_TYPE, user_social_account_type).apply();
    }

    public String getUserSocialAccountType() {
        return preference.getString(USER_SOCIAL_ACCOUNT_TYPE, "");
    }

}
