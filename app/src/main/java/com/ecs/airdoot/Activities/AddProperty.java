package com.ecs.airdoot.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ecs.airdoot.Fragments.AddProertyFragment;
import com.ecs.airdoot.Fragments.DashboardFragment;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.BasicUtils;
import com.ecs.airdoot.Utils.PreferenceManager;

public class AddProperty extends AppCompatActivity {

    Context context;
    public static Toolbar toolbar;
    public static TextView tv_toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_property);

        ButterKnife.bind(this);
        context = AddProperty.this;

        toolbar = findViewById(R.id.toolbar);


        initilizeData();
        defaultFragment();
    }

    private void initilizeData() {
        tv_toolbar_title = toolbar.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText("Add Property");


    }

    private void defaultFragment() {
        Fragment fragment = null;
        fragment = new AddProertyFragment();
        BasicUtils.replaceFragment(context, fragment);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}