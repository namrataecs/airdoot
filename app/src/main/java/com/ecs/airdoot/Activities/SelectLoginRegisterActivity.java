package com.ecs.airdoot.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.PreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectLoginRegisterActivity extends AppCompatActivity {

    Context context;
    PreferenceManager preferenceManager;
    public static Toolbar toolbar;
    public static TextView tv_ToolbarTitle;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.btn_regi)
    Button btnRegi;

    @BindView(R.id.tv_guest)
    TextView tvGuest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectloginregister);
        toolbar = findViewById(R.id.toolbar);

        ButterKnife.bind(this);
        context = SelectLoginRegisterActivity.this;
        preferenceManager = PreferenceManager.getInstance(this);
        initilizeData();

        setUpEvents();


    }

    private void initilizeData() {

    }

    private void setUpEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(context, TechnicianDashboardActivity.class);
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });

        btnRegi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, RegistrationActivity.class);
                startActivity(intent);
            }
        });
        tvGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });


    }
}
