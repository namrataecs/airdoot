package com.ecs.airdoot.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.ecs.airdoot.Fragments.Technician.MyTaskFragment;
import com.ecs.airdoot.Fragments.Technician.TodayReportFragment;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.BasicUtils;
import com.ecs.airdoot.Utils.PreferenceManager;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TechnicianDashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Context context;
    PreferenceManager preferenceManager;
    public static Toolbar toolbar;
    public static TextView tv_toolbar_title;


    @BindView(R.id.drawer_tech_layout)
    DrawerLayout drawerTechLayout;

    @BindView(R.id.nav_tech_view)
    NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_technician_dashboard);


        ButterKnife.bind(this);
        context = TechnicianDashboardActivity.this;

        toolbar = findViewById(R.id.toolbar);

        setNavigationDrawer();
        initilizeData();
        setUpEvents();
        defaultFragment();
    }

    private void defaultFragment() {
        Fragment fragment = null;
        fragment = new MyTaskFragment();
        BasicUtils.replaceFragment(context, fragment);
    }

    private void initilizeData() {
        tv_toolbar_title = toolbar.findViewById(R.id.tv_toolbar_title);
        View headerView = navigationView.getHeaderView(0);
        //TextView navUsername = (TextView) headerView.findViewById(R.id.txtHeaderName);
        //CircleImageView imgProfile = (CircleImageView) headerView.findViewById(R.id.img_profile);
        preferenceManager = PreferenceManager.getInstance(this);

    }

    private void setUpEvents() {

    }

    private void setNavigationDrawer() {
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerTechLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerTechLayout.addDrawerListener(toggle);
        toggle.syncState();
//        Set color red to Logout text in navigation drawer
        SpannableString spannableString = new SpannableString("Logout");
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, spannableString.length(), 0);

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_tech_drawer, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Log.e("dggkasjhdsajh", "am,hasjkhjdk");

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_complain_status) {

            BasicUtils.replaceFragment(context, new TodayReportFragment());

            //  BasicUtils.replaceFragment(PatientMainActivity.this, new ContactFragment());
        }
        if (id == R.id.nav_home) {

            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
            finish();

            //  BasicUtils.replaceFragment(PatientMainActivity.this, new ContactFragment());
        } else if (id == R.id.nav_mytask) {
            defaultFragment();

        } else if (id == R.id.nav_need_help) {


        } else if (id == R.id.nav_logout) {

        }


        //DrawerLayout drawer = findViewById(R.id.drawer_tech_layout);
        drawerTechLayout.closeDrawer(GravityCompat.END);
        return true;
    }


}
