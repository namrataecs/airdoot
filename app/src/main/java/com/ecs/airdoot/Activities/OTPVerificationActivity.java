package com.ecs.airdoot.Activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.PreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OTPVerificationActivity extends AppCompatActivity {

    Context context;
    PreferenceManager preferenceManager;
    public static Toolbar toolbar;
    public static TextView tv_ToolbarTitle;
    @BindView(R.id.btn_cancel)
    AppCompatButton btn_cancel;

    @BindView(R.id.iv_back)
    AppCompatImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);
        toolbar = findViewById(R.id.toolbar);

        ButterKnife.bind(this);
        context = OTPVerificationActivity.this;
        preferenceManager = PreferenceManager.getInstance(this);
        initilizeData();

        setUpEvents();


    }

    private void initilizeData() {


    }

    private void setUpEvents() {
       /* btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });*/

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
