package com.ecs.airdoot.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.ecs.airdoot.Interface.VolleyResponse;
import com.ecs.airdoot.Model.CheckExistingUserRepsponse;
import com.ecs.airdoot.Model.LoginRepsponse;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Services.HttpService;
import com.ecs.airdoot.Utils.Constants;
import com.ecs.airdoot.Utils.MU;
import com.ecs.airdoot.Utils.PreferenceManager;
import com.ecs.airdoot.Utils.Utils;
import com.ecs.airdoot.Utils.VU;
import com.ecs.airdoot.database.DataBaseHelper;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    Context context;
    PreferenceManager preferenceManager;
    public static Toolbar toolbar;
    public static TextView tv_ToolbarTitle;

    @BindView(R.id.edt_email_or_number)
    EditText edtEmailOrNumber;

    @BindView(R.id.edt_password)
    EditText edtPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;

    @BindView(R.id.chk_remember_me)
    CheckBox chkRememberMe;

    @BindView(R.id.login_button)
    LoginButton loginButton;

    @BindView(R.id.facebookView)
    Button facebookView;

    @BindView(R.id.gmail_view)
    Button gmailView;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    String TAG = "LoginActivity";
    JSONArray jsonUserArray = null;

    //a constant for detecting the login intent result
    private static final int RC_SIGN_IN = 234;

    private CallbackManager callbackManager;
    private AccessToken accessToken;

    String str_Name = "", str_BirthDate = "", str_ID = "", str_Image_Url = " ", str_Email = "", str_Gender = "";

    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;

    //And also a Firebase Auth object
    FirebaseAuth mAuth;
    DataBaseHelper dataBaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        toolbar = findViewById(R.id.toolbar);

        ButterKnife.bind(this);
        context = LoginActivity.this;

        printHashKey(context);
        preferenceManager = PreferenceManager.getInstance(this);
        initilizeData();
        setUpEvents();
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("Key_Hash_Log", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initilizeData() {
        Log.e("activity_name_Log", "LoginActivity" + preferenceManager.getPassword());

        if (!preferenceManager.getEmailId().equals("") || preferenceManager.getEmailId().contains("@")) {
            edtEmailOrNumber.setText(preferenceManager.getEmailId());
        } else if (!preferenceManager.getContactNo().equals("") || !preferenceManager.getEmailId().contains("@")) {
            edtEmailOrNumber.setText(preferenceManager.getContactNo());
        }

        if (!preferenceManager.getPassword().equals("")) {
            edtPassword.setText(preferenceManager.getPassword());
        } else {
            edtEmailOrNumber.setText("");
            edtPassword.setText("");
        }

        dataBaseHelper = new DataBaseHelper(context);
        accessToken = AccessToken.getCurrentAccessToken();
        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();

        //Then we need a GoogleSignInOptions object
        //And we need to build it as below
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                if (AccessToken.getCurrentAccessToken() == null) {
                    System.out.println("not logged in yet");
                } else {
                    System.out.println("Logged in");
                }
            }
        });

        loginButton.setReadPermissions(Arrays.asList(new String[]{"email"}));
//        loginButton.setReadPermissions(Arrays.asList(new String[]{"email", "user_birthday", "user_hometown", "public_profile", "user_gender"}));


        if (accessToken != null) {
            getProfileData();
        }

        //        Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("LoginButton", "User login successfully");
                getProfileData();
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("LoginButton", "User cancel login");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("LoginButton", "Problem for login");
            }
        });

    }

    private void getProfileData() {
        try {
            accessToken = AccessToken.getCurrentAccessToken();
            GraphRequest request;
            request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.d("faceBookLogin", "Graph Object :" + object);

                            try {
                                str_Name = object.getString("name");
                                str_ID = object.getString("id");
                                // str_Email = object.getString("email");
                                // str_Image_Url = "https://graph.facebook.com/" + str_ID + "/picture?type=normal";
                                //  str_Gender = object.getString("gender");
                                //  str_BirthDate = object.getString("birthday");

                                if (!object.has("birthday")) {
                                    Log.d("birthday", ":" + "birthday");
                                    str_BirthDate = "";
                                } else {
                                    str_BirthDate = object.getString("birthday");
                                }
                                if (!object.has("gender")) {
                                    Log.d("gender", ":" + "gender");
                                    str_Gender = "";

                                } else {
                                    str_Gender = object.getString("gender");
                                }

                                /*if (!object.has("https://graph.facebook.com/" + str_ID + "/picture?type=normal")) {
                                    Log.d("Image", ":" + "https://graph.facebook.com/" + str_ID + "/picture?type=normal");
                                    str_Image_Url = "https://graph.facebook.com/" + str_ID + "/picture?type=normal";

                                } else {*/
//                                str_Image_Url = object.getString("https://graph.facebook.com/" + str_ID + "/picture?type=normal");

                                str_Image_Url = "https://graph.facebook.com/" + str_ID + "/picture?type=large";

//                                }
                                if (!object.has("email")) {
                                    Log.d("Image", ":" + "email");
                                    str_Email = "";

                                } else {
                                    str_Email = object.getString("email");
                                }

                                Log.e("str_ID_FB", ":" + str_ID);
                                Log.e("str_Name_FB", ":" + str_Name);
                                Log.e("str_Email_FB", ":" + str_Email);
                                Log.e("str_BirthDate_FB", ":" + str_BirthDate);
                                Log.e("str_Gender_FB", ":" + str_Gender);
                                Log.e("str_Image_Url_FB", ":" + str_Image_Url);

                                //checkISExist("Facebook");
                                PreferenceManager.getInstance(context).setIsLoggedIn(true);
                                PreferenceManager.getInstance(context).setUserSocialAccountType("Facebook");
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,link,birthday,gender,email");
            request.setParameters(parameters);
            request.executeAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                jsonUserArray = new JSONArray();
              /*  if (Validate()) {
                    if (chkRememberMe.isChecked()) {
                        if (edtEmailOrNumber.getText().toString().contains("@")) {
                            rememberMe(edtEmailOrNumber.getText().toString(), edtPassword.getText().toString(), "email");
                            jsonUserArray = dataBaseHelper.getUserDataByEmail(edtEmailOrNumber.getText().toString());
                            checkLogin("email");
                        } else {
                            rememberMe(edtEmailOrNumber.getText().toString(), edtPassword.getText().toString(), "mobile_number");
                            jsonUserArray = dataBaseHelper.getUserDataBynumber(edtEmailOrNumber.getText().toString(), edtPassword.getText().toString());
                            checkLogin("moible");
                        }
                    } else {
                        rememberMe("", "", "");

                        if (edtEmailOrNumber.getText().toString().contains("@")) {
                            rememberMe(edtEmailOrNumber.getText().toString(), edtPassword.getText().toString(), "email");
                            jsonUserArray = dataBaseHelper.getUserDataByEmail(edtEmailOrNumber.getText().toString());
                            checkLogin("email");
                        } else {
                            rememberMe(edtEmailOrNumber.getText().toString(), edtPassword.getText().toString(), "mobile_number");
                            jsonUserArray = dataBaseHelper.getUserDataBynumber(edtEmailOrNumber.getText().toString(), edtPassword.getText().toString());
                            checkLogin("moible");
                        }
                    }

                   *//* if (jsonUserArray != null && jsonUserArray.length() > 0) {
                        //   [{"id":"1","email_address":"","mobile_number":"8871223023","password":"1234","otp":"","created_by":"","created_date_time":"","is_deleted":"N"}]
                        PreferenceManager.getInstance(context).setUserSocialAccountType("Normal");
                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(intent);
                    } else {
                        MU.ShowToast(context, "Please Register first");
                    }*//*
                }*/
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        gmailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        facebookView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void checkLogin(String type) {
        Log.e("type_of_login", ":" + type);
        if (Utils.isConnectingToInternet(context)) {
            JSONObject jsonObject = null;
            try {
                Map<String, String> headerParams = new HashMap<>();
                Map<String, String> requestBodyParams = new HashMap<>();
                jsonObject = new JSONObject();
                if (type.equals("email")) {
                    jsonObject.put("email_id", edtEmailOrNumber.getText().toString());
                    jsonObject.put("mobile_number", "");
                } else {
                    jsonObject.put("email_id", "");
                    jsonObject.put("mobile_number", edtEmailOrNumber.getText().toString().trim());
                }

                jsonObject.put("log_in_password", edtPassword.getText().toString().trim());

                Log.e("params_userlogin", "" + jsonObject);
                Log.e("url_userlogin", "" + Constants.LOGIN_CHECK);

                HttpService.accessWebServicesJSON(
                        context,
                        Constants.LOGIN_CHECK,
                        Request.Method.POST,
                        jsonObject,
                        new VolleyResponse() {
                            @Override

                            public void onProcessFinish(String response, VolleyError error, String status) {

                                try {
                                    Log.e("Error_Login", ":" + error);
                                    Log.e("Status_Login", ":" + status);
                                    Log.e("response_Login", ":" + response);

                                    Intent intent = new Intent(context, MainActivity.class);
                                    startActivity(intent);
                                    if (status.equals("response")) {

                                        LoginRepsponse loginRepsponse = (LoginRepsponse) Utils.parseResponse(response, LoginRepsponse.class);
                                        if (!loginRepsponse.getMessage().equals("fail")) {
                                            preferenceManager.setStatus(String.valueOf(loginRepsponse.getData().getStatus()));
                                            preferenceManager.setUserType(loginRepsponse.getData().getUserType());
                                            preferenceManager.setApplicationId(loginRepsponse.getData().getApplicationId());
                                            MU.ShowToast(context, "Login successfully");
                                            PreferenceManager.getInstance(context).setUserSocialAccountType("Normal");
                                         /*   Intent intent = new Intent(context, MainActivity.class);
                                            startActivity(intent);
*/
                                        } else {
                                            MU.ShowToast(context, "Please Enter Valid Username and Password ");
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            MU.ShowToast(context, MU.NO_INTERNET_CONNECTION_TRY_AGAIN);
        }

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();

        //starting the activity for result
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if the requestCode is the Google Sign In code that we defined at starting
        if (requestCode == RC_SIGN_IN) {

            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //authenticating with firebase
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
//                Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e("requestCode_Log", ":" + requestCode);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        //Now using firebase we are signing in the user here
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            str_ID = user.getUid();
                            str_Name = user.getDisplayName();
                            str_Email = user.getEmail();
                            Uri uri = user.getPhotoUrl();
                            str_Image_Url = uri.toString();

                            Log.e("str_Name_Log", ":" + str_Name);
                            Log.e("str_ID_Log", ":" + str_ID);
                            Log.e("str_BirthDate_Log", ":" + str_BirthDate);
                            Log.e("str_Email_Log", ":" + str_Email);
                            Log.e("str_Gender_Log", ":" + str_Gender);
                            Log.e("str_Image_Url_Log", ":" + str_Image_Url);

                            PreferenceManager.getInstance(context).setIsLoggedIn(true);
                            PreferenceManager.getInstance(context).setUserSocialAccountType("Google");


                            checkISExist(str_Email);

                          /*  Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);*/
                            finish();
                            Log.e("user_Name_Log", ":" + user.getDisplayName());
                            Log.e("str_Email", ":" + str_Email);
                            Log.e("uri", ":" + uri);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(context, "Authentication failed.", Toast.LENGTH_SHORT).show();

                        }


                    }
                });
    }

    private void checkISExist(String email) {
        if (Utils.isConnectingToInternet(context)) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("email_id", email);
                Constants.CHECK_EXISTING_USER = Constants.CHECK_AVAILABILITY_EMAIL;
                Log.e("params_exist", "" + jsonObject);
                Log.e("url_exist", "" + Constants.CHECK_EXISTING_USER);

                HttpService.accessWebServicesJSON(
                        context,
                        Constants.CHECK_EXISTING_USER,
                        Request.Method.POST,
                        jsonObject,
                        new VolleyResponse() {
                            @Override

                            public void onProcessFinish(String response, VolleyError error, String status) {

                                try {
                                    Log.e("Error_exist", ":" + error);
                                    Log.e("Status_exist", ":" + status);
                                    Log.e("response_exist", ":" + response);

                                    if (status.equals("response")) {

                                        CheckExistingUserRepsponse checkExistingUserRepsponse = (CheckExistingUserRepsponse) Utils.parseResponse(response, CheckExistingUserRepsponse.class);
                                        Log.e("response_existReg", ":" + response);

                                        if (!checkExistingUserRepsponse.getMessage().equals("fail")) {
                                            Log.e("response_existReg_if_condition", ":" + response);
                                            //  UserRegistration("email");

                                        } else {
                                            MU.ShowToast(context, "User Already exists");
                                        }


                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            MU.ShowToast(context, MU.NO_INTERNET_CONNECTION_TRY_AGAIN);
        }
    }


    private boolean Validate() {
        if (VU.isEmpty(edtEmailOrNumber)) {
            edtEmailOrNumber.setError(getResources().getString(R.string.please_enter_email_address_or_number));
            edtEmailOrNumber.requestFocus();
            return false;

        } else if (edtEmailOrNumber.getText().toString().contains("@") && VU.isEmailId(edtEmailOrNumber)) {
            edtEmailOrNumber.setError(getResources().getString(R.string.please_enter_valid_email_address));
            edtEmailOrNumber.requestFocus();
            return false;
        } else if (!edtEmailOrNumber.getText().toString().contains("@") && VU.isContactNo(edtEmailOrNumber)) {
            edtEmailOrNumber.setError(getResources().getString(R.string.please_enter_valid_email_address));
            edtEmailOrNumber.requestFocus();
            return false;

        } else if (VU.isEmpty(edtPassword)) {
            edtPassword.setError(getResources().getString(R.string.please_enter_password));
            edtPassword.requestFocus();
            return false;
        }
        return true;


    }

    private void rememberMe(String emailorno, String Password, String type) {
        Log.e("checked_data", ":" + " email- " + emailorno + " password- " + Password + " type- " + type);
        if (type.equals("email")) {
            PreferenceManager.getInstance(context).setEmailId(emailorno);

        } else {
            PreferenceManager.getInstance(context).setContactNo(emailorno);
        }

        PreferenceManager.getInstance(context).setPassword(Password);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, SelectLoginRegisterActivity.class));
        finish();
    }
}
