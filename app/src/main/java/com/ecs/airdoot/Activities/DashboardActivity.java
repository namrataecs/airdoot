package com.ecs.airdoot.Activities;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.PreferenceManager;

import butterknife.ButterKnife;

public class DashboardActivity extends AppCompatActivity {

    Context context;
    PreferenceManager preferenceManager;
    public static Toolbar toolbar;
    public static TextView tv_ToolbarTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        toolbar = findViewById(R.id.toolbar);

        ButterKnife.bind(this);
        context = DashboardActivity.this;
        preferenceManager = PreferenceManager.getInstance(this);
        initilizeData();

        setUpEvents();


    }

    private void initilizeData() {


    }

    private void setUpEvents() {

    }
}
