package com.ecs.airdoot.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.ecs.airdoot.Fragments.AddEquipmentFragment;
import com.ecs.airdoot.Fragments.DashboardFragment;
import com.ecs.airdoot.Fragments.ProfileFragment;
import com.ecs.airdoot.Fragments.ViewPropertytFragment;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.BasicUtils;
import com.ecs.airdoot.Utils.PreferenceManager;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Context context;
    PreferenceManager preferenceManager;

    public static Toolbar toolbar;

    public static TextView tv_toolbar_title;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.ll_bottom_home)
    LinearLayoutCompat ll_bottom_home;

    @BindView(R.id.ll_bottom_profile)
    RelativeLayout ll_bottom_profile;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.iv_home)
    ImageView iv_home;
    @BindView(R.id.iv_my_offer)
    ImageView iv_my_offer;
    @BindView(R.id.iv_notification)
    ImageView iv_notification;
    @BindView(R.id.iv_my_profile)
    ImageView iv_my_profile;

    private static final String TAG = "MainActivity";
    private static final String ARG_NAME = "username";

    final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    android.app.Dialog closeAppDialog;
    FirebaseAuth firebaseAuth;
    GoogleSignInClient googleSignInClient;


    public static void startActivity(Context context, String username) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(ARG_NAME, username);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;

        toolbar = findViewById(R.id.toolbar);
        ButterKnife.bind(this);


        setNavigationDrawer();
        initilizeData();
        defaultFragment();
        setUpEvents();
    }

    private void setUpEvents() {
        ll_bottom_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultFragment();
            }
        });

        ll_bottom_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //bottomNavigationColorChange(iv_my_profile,iv_home,iv_my_offer,iv_notification);
                Fragment fragment = null;
                fragment = new ProfileFragment();
                BasicUtils.replaceFragment(context, fragment);
            }
        });
    }

    private void bottomNavigationColorChange(ImageView img_first, ImageView img_second, ImageView img_third, ImageView img_fourth) {
        img_first.setColorFilter(ContextCompat.getColor(context, R.color.dark_grey), android.graphics.PorterDuff.Mode.SRC_IN);
        img_second.setColorFilter(ContextCompat.getColor(context, R.color.colorlight_grey), android.graphics.PorterDuff.Mode.SRC_IN);
        img_third.setColorFilter(ContextCompat.getColor(context, R.color.colorlight_grey), android.graphics.PorterDuff.Mode.SRC_IN);
        img_fourth.setColorFilter(ContextCompat.getColor(context, R.color.colorlight_grey), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void defaultFragment() {
        //bottomNavigationColorChange(iv_home,iv_my_offer,iv_notification,iv_my_profile);
        Fragment fragment = null;
        fragment = new DashboardFragment();
        BasicUtils.replaceFragment(context, fragment);
    }

    private void initilizeData() {
        tv_toolbar_title = toolbar.findViewById(R.id.tv_toolbar_title);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        // TextView navUsername = (TextView) headerView.findViewById(R.id.txtHeaderName);
        //     CircleImageView imgProfile = (CircleImageView) headerView.findViewById(R.id.img_profile);
        preferenceManager = PreferenceManager.getInstance(this);

        onClickNavMenu();

       /* if (!preferenceManager.getFirstName().equals("")) {
            navUsername.setText(preferenceManager.getFirstName() + " " + preferenceManager.getLastName());
        }
        if (!preferenceManager.getPhoto().equals("")) {
            IU.ImageLoader_Circular(context, preferenceManager.getPhoto(), imgProfile);
        }*/

    }

    private void onClickNavMenu() {
        ImageView img_close_drawer = (ImageView) navigationView.findViewById(R.id.img_close_drawer);
        TextView tv_home = (TextView) navigationView.findViewById(R.id.tv_home);
        TextView tv_add_asset = (TextView) navigationView.findViewById(R.id.tv_add_asset);
        TextView tv_add_property = (TextView) navigationView.findViewById(R.id.tv_add_property);
        TextView tv_logout = (TextView) navigationView.findViewById(R.id.tv_logout);

        img_close_drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultFragment();
                drawerLayout.closeDrawer(GravityCompat.END);
            }
        });
        tv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultFragment();
                drawerLayout.closeDrawer(GravityCompat.END);
            }
        });


        tv_add_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasicUtils.replaceFragment(context, new AddEquipmentFragment());
            }
        });

        tv_add_property.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasicUtils.replaceFragment(context, new ViewPropertytFragment());
            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.getInstance(context).setIsLoggedIn(false);
                Log.e("islogin=>", ":" + preferenceManager.getIsLoggedIn());


                Log.e("UserAcc_Type", ":" + PreferenceManager.getInstance(context).getUserSocialAccountType());

                if (PreferenceManager.getInstance(context).getUserSocialAccountType().equals("Facebook")) {

                    FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
                        @Override
                        public void onInitialized() {
                            if (AccessToken.getCurrentAccessToken() == null) {
                                Log.e("FAcebookLogin_Logout", "  :FAlse:  ");
                            } else {
                                Log.e("FAcebookLogin_Logout", "  :true:  ");

                                LoginManager.getInstance().logOut();

                                PreferenceManager.getInstance(context).clear();
                                finish();
                                startActivity(new Intent(context, SelectLoginRegisterActivity.class));

                            }
                        }
                    });


                } else if (PreferenceManager.getInstance(context).getUserSocialAccountType().equals("Google")) {

                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build();
                    GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(context, gso);
                    googleSignInClient.signOut();

                    PreferenceManager.getInstance(context).clear();
                    finish();


                    Intent nextScreen = new Intent(context, SelectLoginRegisterActivity.class);
                    nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(nextScreen);

//                                startActivity(new Intent(context, LoginActivity.class));

                } else {
                    //     PreferenceManager.getInstance(context).clear();
                    Intent nextScreen = new Intent(context, SelectLoginRegisterActivity.class);
                    nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(nextScreen);

//                                startActivity(new Intent(context, LoginActivity.class));
                    finish();
                }

            }
        });

    }

    private void setNavigationDrawer() {
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

//        Set color red to Logout text in navigation drawer
        SpannableString spannableString = new SpannableString("Logout");
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, spannableString.length(), 0);

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Log.e("dggkasjhdsajh", "am,hasjkhjdk");

        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            defaultFragment();
            //  BasicUtils.replaceFragment(PatientMainActivity.this, new ContactFragment());
        } else if (id == R.id.nav_my_profile) {
            BasicUtils.replaceFragment(context, new ProfileFragment());

        } else if (id == R.id.nav_technician) {
            Intent intent = new Intent(context, TechnicianDashboardActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_add_asset) {
            BasicUtils.replaceFragment(context, new AddEquipmentFragment());
        } else if (id == R.id.nav_add_property) {
            BasicUtils.replaceFragment(context, new ViewPropertytFragment());
        } else if (id == R.id.nav_support) {

        } else if (id == R.id.nav_logout) {


            PreferenceManager.getInstance(context).setIsLoggedIn(false);
            Log.e("islogin=>", ":" + preferenceManager.getIsLoggedIn());


            Log.e("UserAcc_Type", ":" + PreferenceManager.getInstance(context).getUserSocialAccountType());

            if (PreferenceManager.getInstance(context).getUserSocialAccountType().equals("Facebook")) {

                FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
                    @Override
                    public void onInitialized() {
                        if (AccessToken.getCurrentAccessToken() == null) {
                            Log.e("FAcebookLogin_Logout", "  :FAlse:  ");
                        } else {
                            Log.e("FAcebookLogin_Logout", "  :true:  ");

                            LoginManager.getInstance().logOut();

                            PreferenceManager.getInstance(context).clear();
                            finish();
                            startActivity(new Intent(context, SelectLoginRegisterActivity.class));

                        }
                    }
                });


            } else if (PreferenceManager.getInstance(context).getUserSocialAccountType().equals("Google")) {

                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build();
                GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(context, gso);
                googleSignInClient.signOut();

                PreferenceManager.getInstance(context).clear();
                finish();


                Intent nextScreen = new Intent(context, SelectLoginRegisterActivity.class);
                nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(nextScreen);

//                                startActivity(new Intent(context, LoginActivity.class));

            } else {
                //     PreferenceManager.getInstance(context).clear();
                Intent nextScreen = new Intent(context, SelectLoginRegisterActivity.class);
                nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(nextScreen);

//                                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }


    @Override
    public void onBackPressed() {
        String tag = getCurrentFragment();
        Log.e("Fragment_Tag_Log", ":" + tag);
        int count = getSupportFragmentManager().getBackStackEntryCount();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

        if (tag != null && tag.equals(DashboardFragment.class.getName())) {
//            super.onBackPressed();
            Log.e("DashFragmentsfasdf", "OnBack");

            appCloseDialog();
        } else {
            if (fragment instanceof DashboardFragment) {
                for (int i = 0; i < count; ++i) {
                    getFragmentManager().popBackStackImmediate();
                }
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                return;
            } else {
                if (count == 1) {
                    appCloseDialog();

                } else {
                    getSupportFragmentManager().popBackStack();
                }
            }
        }
    }


    private String getCurrentFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = null;
        String fragmentTag = null;
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
            currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
        }
        return fragmentTag;
    }

    public void appCloseDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.close_app_dialog, null);
        TextView tvCancel = alertLayout.findViewById(R.id.tv_cancel);
        TextView tvYes = alertLayout.findViewById(R.id.tv_yes);

        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        //  alert.setTitle("Logout");

        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();


        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finishAffinity();
            }
        });
    }


}