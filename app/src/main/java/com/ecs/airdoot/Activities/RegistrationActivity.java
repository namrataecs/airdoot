package com.ecs.airdoot.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.ecs.airdoot.Interface.VolleyResponse;
import com.ecs.airdoot.Model.CheckExistingUserRepsponse;
import com.ecs.airdoot.Model.LoginRepsponse;
import com.ecs.airdoot.R;
import com.ecs.airdoot.Services.HttpService;
import com.ecs.airdoot.Utils.Constants;
import com.ecs.airdoot.Utils.DTU;
import com.ecs.airdoot.Utils.MU;
import com.ecs.airdoot.Utils.PreferenceManager;
import com.ecs.airdoot.Utils.Utils;
import com.ecs.airdoot.Utils.VU;
import com.ecs.airdoot.database.DataBaseHelper;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegistrationActivity extends AppCompatActivity {

    Context context;
    PreferenceManager preferenceManager;
    public static Toolbar toolbar;
    public static TextView tv_ToolbarTitle;

    @BindView(R.id.edt_email_or_number)
    EditText edtEmailOrNumber;

    @BindView(R.id.edt_password)
    EditText edtPassword;

    @BindView(R.id.btn_next)
    AppCompatButton btnNext;

    @BindView(R.id.btn_cancel)
    AppCompatButton btn_cancel;

    @BindView(R.id.iv_back)
    AppCompatImageView iv_back;

    DataBaseHelper dataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regi);
        toolbar = findViewById(R.id.toolbar);

        ButterKnife.bind(this);
        context = RegistrationActivity.this;
        preferenceManager = PreferenceManager.getInstance(this);
        initilizeData();

        setUpEvents();


    }

    private void initilizeData() {
        dataBaseHelper = new DataBaseHelper(context);

    }

    private void setUpEvents() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OTPVerificationActivity.class);
                startActivity(intent);

                /*if (Validate()) {

                    ContentValues cartContentValues = new ContentValues();

                    if (edtEmailOrNumber.getText().toString().contains("@")) {
                        cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.EMAIL_ADDRESS, edtEmailOrNumber.getText().toString());
                    } else {
                        cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.EMAIL_ADDRESS, "");

                    }
                    if (!edtEmailOrNumber.getText().toString().contains("@")) {
                        cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.MOBILE_NUMBER, edtEmailOrNumber.getText().toString());
                    } else {
                        cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.MOBILE_NUMBER, "");

                    }

                    cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.PASSWORD, edtPassword.getText().toString());
                    cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.OTP, "");
                    cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.CREATED_BY, "");
                    cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.IS_DELETED, "N");
                    cartContentValues.put(DataBaseConstants.Constants_TBL_USER_DETAILS.CREATED_DATE_TIME, "");

                    dataBaseHelper.saveToLocalTable(DataBaseConstants.TableNames.TBL_USER_DETAILS, cartContentValues);

                    if (edtEmailOrNumber.getText().toString().contains("@")) {

                        CheckIfExist("email");
                    } else {
                        CheckIfExist("moible");

                    }

                }*/
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void CheckIfExist(String type) {
        if (Utils.isConnectingToInternet(context)) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                if (type.equals("email")) {
                    jsonObject.put("email_id", edtEmailOrNumber.getText().toString());
                    Constants.CHECK_EXISTING_USER = Constants.CHECK_AVAILABILITY_EMAIL;
                } else {
                    jsonObject.put("check_availability_contact_no", edtEmailOrNumber.getText().toString().trim());
                    Constants.CHECK_EXISTING_USER = Constants.CHECK_AVAILABILITY_CONTACT_NO;
                }

                Log.e("params_exist", "" + jsonObject);
                Log.e("url_exist", "" + Constants.CHECK_EXISTING_USER);

                HttpService.accessWebServicesJSON(
                        context,
                        Constants.CHECK_EXISTING_USER,
                        Request.Method.POST,
                        jsonObject,
                        new VolleyResponse() {
                            @Override

                            public void onProcessFinish(String response, VolleyError error, String status) {

                                try {
                                    Log.e("Error_exist", ":" + error);
                                    Log.e("Status_exist", ":" + status);
                                    Log.e("response_exist", ":" + response);

                                    if (status.equals("response")) {

                                        CheckExistingUserRepsponse checkExistingUserRepsponse = (CheckExistingUserRepsponse) Utils.parseResponse(response, CheckExistingUserRepsponse.class);
                                        Log.e("response_existReg", ":" + response);

                                        if (!checkExistingUserRepsponse.getMessage().equals("fail")) {

                                            if (edtEmailOrNumber.getText().toString().contains("@")) {
                                                UserRegistration("email");

                                            } else {
                                                UserRegistration("moible");
                                            }
                                        } else {
                                            MU.ShowToast(context, "User Already exists");
                                        }


                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            MU.ShowToast(context, MU.NO_INTERNET_CONNECTION_TRY_AGAIN);
        }
    }

    private void UserRegistration(String type) {
        Log.e("type_of_login", ":" + type);
        if (Utils.isConnectingToInternet(context)) {
            JSONObject jsonObject = null;
            try {
                Map<String, String> headerParams = new HashMap<>();
                Map<String, String> requestBodyParams = new HashMap<>();
                jsonObject = new JSONObject();
                if (type.equals("email")) {
                    jsonObject.put("email_id", edtEmailOrNumber.getText().toString());
                    jsonObject.put("mobile_number", "");
                } else {
                    jsonObject.put("email_id", "");
                    jsonObject.put("mobile_number", edtEmailOrNumber.getText().toString().trim());
                }
                jsonObject.put("password", edtPassword.getText().toString().trim());
                jsonObject.put("usertype", "");
                jsonObject.put("is_active", "");
                jsonObject.put("created_time", DTU.getCurrentDateTimeStamp(DTU.HMS));
                jsonObject.put("application_id", "Airdoot");

                Log.e("params_userRegister", "" + jsonObject);
                Log.e("url_userRegister", "" + Constants.TBL_LOGIN);

                HttpService.accessWebServicesJSON(
                        context,
                        Constants.TBL_LOGIN,
                        Request.Method.POST,
                        jsonObject,
                        new VolleyResponse() {
                            @Override

                            public void onProcessFinish(String response, VolleyError error, String status) {

                                try {
                                    Log.e("Error_Register", ":" + error);
                                    Log.e("Status_Register", ":" + status);
                                    Log.e("response_Register", ":" + response);

                                    if (status.equals("response")) {

                                        LoginRepsponse loginRepsponse = (LoginRepsponse) Utils.parseResponse(response, LoginRepsponse.class);
                                        if (loginRepsponse != null) {
                                            MU.ShowToast(context, "Register successfully");
                                            Intent intent = new Intent(context, LoginActivity.class);
                                            startActivity(intent);
                                        } else {
                                            MU.ShowToast(context, "Please Enter Valid Username and Password ");
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            MU.ShowToast(context, MU.NO_INTERNET_CONNECTION_TRY_AGAIN);
        }

    }

    private boolean Validate() {
        if (VU.isEmpty(edtEmailOrNumber)) {
            edtEmailOrNumber.setError(getResources().getString(R.string.please_enter_email_address_or_number));
            edtEmailOrNumber.requestFocus();
            return false;

        } else if (edtEmailOrNumber.getText().toString().contains("@") && VU.isEmailId(edtEmailOrNumber)) {
            edtEmailOrNumber.setError(getResources().getString(R.string.please_enter_valid_email_address));
            edtEmailOrNumber.requestFocus();
            return false;
        } else if (!edtEmailOrNumber.getText().toString().contains("@") && VU.isContactNo(edtEmailOrNumber)) {
            edtEmailOrNumber.setError(getResources().getString(R.string.please_enter_valid_email_address));
            edtEmailOrNumber.requestFocus();
            return false;

        } else if (VU.isEmpty(edtPassword)) {
            edtPassword.setError(getResources().getString(R.string.please_enter_password));
            edtPassword.requestFocus();
            return false;
        }
        return true;


    }

}
