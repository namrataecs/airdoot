package com.ecs.airdoot.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ecs.airdoot.R;
import com.ecs.airdoot.Utils.PreferenceManager;


public class SplashActivity extends AppCompatActivity {

    private Context context = SplashActivity.this;
    private Handler splashHandler;
    String DynamicUrl = "";
    PreferenceManager preferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        splashHandler = new Handler();
        preferenceManager = PreferenceManager.getInstance(this);
        // CheckUrl("ghatgroup");

        Log.e("islogin=>", ":" + preferenceManager.getIsLoggedIn());
        if (preferenceManager.getIsLoggedIn() == true) {
            final Intent mainIntent = new Intent(context, MainActivity.class);
            startActivity(mainIntent);
            finish();

        } else {
            final Intent mainIntent = new Intent(context, SelectLoginRegisterActivity.class);
            startActivity(mainIntent);
        }


        splashHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashActivity.this.finish();
            }
        }, 2000);

    }

}
